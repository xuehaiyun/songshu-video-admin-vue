import JSencrypt from './encrypt'
import {frontURL} from "../api/config";

const utils = {
  formatUrl:(url)=>{
    if (url == undefined) {
      return;
    }
    var index = url.indexOf("http");
    if (index == 0) {
      return url;
    }
    return frontURL + url;
  }
}
export default utils;
