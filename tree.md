.
|-- README.en.md
|-- README.md
|-- build
|   |-- build.js
|   |-- check-versions.js
|   |-- logo.png
|   |-- utils.js
|   |-- vue-loader.conf.js
|   |-- webpack.base.conf.js
|   |-- webpack.dev.conf.js
|   `-- webpack.prod.conf.js
|-- config
|   |-- dev.env.js
|   |-- index.js
|   `-- prod.env.js
|-- index.html
|-- package-lock.json
|-- package.json
|-- report.20200205.211754.15796.0.001.json
|-- songshu-video-admin-vue.iml
|-- src
|   |-- App.vue
|   |-- api
|   |   |-- config.js
|   |   `-- index.js
|   |-- assets
|   |   |-- css
|   |   |   |-- color-dark.css
|   |   |   |-- icon.css
|   |   |   |-- main.css
|   |   |   `-- theme-green
|   |   |       |-- color-green.css
|   |   |       |-- fonts
|   |   |       `-- index.css
|   |   |-- img
|   |   |   |-- img.jpg
|   |   |   |-- log.tar.gz
|   |   |   |-- login-bg.jpg
|   |   |   |-- logo.png
|   |   |   |-- qrcode.png
|   |   |   `-- qrcodewx.png
|   |   `-- logo.png
|   |-- components
|   |   |-- HelloWorld.vue
|   |   |-- common
|   |   |   |-- Header.vue
|   |   |   |-- Home.vue
|   |   |   |-- Sidebar.vue
|   |   |   |-- Tags.vue
|   |   |   |-- bus.js
|   |   |   |-- directives.js
|   |   |   `-- i18n.js
|   |   `-- page
|   |       |-- 403.vue
|   |       |-- 404.vue
|   |       |-- Admin
|   |       |   |-- List.vue
|   |       |   `-- Log.vue
|   |       |-- Advert
|   |       |   `-- List.vue
|   |       |-- Config
|   |       |   |-- Aliyun.vue
|   |       |   |-- Basic.vue
|   |       |   |-- Comment.vue
|   |       |   |-- Extra.vue
|   |       |   |-- FTP.vue
|   |       |   |-- Mail.vue
|   |       |   |-- Payment.vue
|   |       |   |-- QQ.vue
|   |       |   |-- QiNiu.vue
|   |       |   |-- SafeCy.vue
|   |       |   |-- Seo.vue
|   |       |   |-- Sms.vue
|   |       |   |-- Video.vue
|   |       |   |-- Wechat.vue
|   |       |   `-- Weibo.vue
|   |       |-- Dashboard.vue
|   |       |-- Login.vue
|   |       |-- Order
|   |       |   `-- List.vue
|   |       |-- TextImage
|   |       |   `-- List.vue
|   |       |-- Type
|   |       |   `-- List.vue
|   |       |-- Update
|   |       |   `-- List.vue
|   |       |-- User
|   |       |   |-- Change.vue
|   |       |   |-- Comment.vue
|   |       |   |-- Invite.vue
|   |       |   |-- List.vue
|   |       |   |-- Log.vue
|   |       |   |-- LogAction.vue
|   |       |   `-- Withdraw.vue
|   |       |-- Video
|   |       |   `-- List.vue
|   |       `-- Vip
|   |           |-- PayType.vue
|   |           `-- VipShop.vue
|   |-- main.js
|   |-- router
|   |   `-- index.js
|   |-- store
|   |   `-- index.js
|   `-- utils
|       |-- encrypt.js
|       |-- index.js
|       `-- request.js
|-- static
|-- tree.md
`-- tree.txt

26 directories, 85 files
